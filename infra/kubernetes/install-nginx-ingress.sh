#!/bin/bash

# https://docs.nginx.com/nginx-ingress-controller/installation/installation-with-helm/

helm repo add nginx-stable https://helm.nginx.com/stable
helm repo update

helm install my-release nginx-stable/nginx-ingress

