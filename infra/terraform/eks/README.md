# Terraform module for EKS

*Warning! AWS charges $0.10 per hour for each EKS cluster.*

This module is not my original work and is mostly based on the Hashicorp tutorial [Provision an EKS Cluster learn guide](https://learn.hashicorp.com/terraform/kubernetes/provision-eks-cluster), containing Terraform configuration files to provision an EKS cluster on AWS.

```
terraform init
terraform plan
terraform apply
```

`./configure-kubectl.sh`
