#!/bin/bash

# When terraform apply finishes, run the following command to retreive access credentials for your cluster and automatically configure kubectl

aws eks --region $(terraform output -raw region) update-kubeconfig --name $(terraform output -raw cluster_name)

# Test new configuration
kubectl cluster-info
kubectl config get-contexts
