# PostgreSQL deployed via AWS RDS

Terraform code to deploy PostgreSQL DB with SG rule for EKS VPC access

Largely based on https://github.com/terraform-aws-modules/terraform-aws-rds/tree/v3.4.1/examples/complete-postgres

```
terraform init
terraform plan
terraform apply
```
