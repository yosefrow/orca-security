#!/bin/bash

POSTGRES_PASSWORD=$(terraform output -raw db_default_instance_password)
POSTGRES_USER=$(terraform output -raw db_default_instance_username)
POSTGRES_DB=$(terraform output -raw db_default_instance_name)
POSTGRES_ENDPOINT=$(terraform output -raw db_default_instance_endpoint)

echo "Retrieving PostgreSQL connection string based on terraform outputs"

# postgresql://[user[:password]@][netloc][:port][/dbname][?param1=value1&...]
# http://www.postgresql.org/docs/current/static/libpq-connect.html#LIBPQ-CONNSTRING

# postgresql+psycopg2://user:password@host:port/dbname[?key=value&key=value...]
# https://docs.sqlalchemy.org/en/14/dialects/postgresql.html
echo DATABASE_URL=postgresql+psycopg2://${POSTGRES_USER}:${POSTGRES_PASSWORD}@${POSTGRES_ENDPOINT}/${POSTGRES_DB}
