#!/bin/bash -eu

REGION=us-east-1
REPO_NAME="$1"

aws ecr create-repository \
    --repository-name $REPO_NAME \
    --image-scanning-configuration scanOnPush=true \
    --region $REGION
