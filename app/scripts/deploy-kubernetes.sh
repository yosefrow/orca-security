#!/bin/bash

SCRIPT_DIR=$(cd $(dirname $0) && pwd); cd $SCRIPT_DIR

cd ../kubernetes
kubectl apply -f files/orca-app-service.yaml
kubectl apply -f files/orca-app-pdb.yaml
#kubectl apply -f files/orca-app-ingress.yaml

source ../.env.prod
export DATABASE_URL
export BUILD_VERSION

envsubst < files/orca-app-deployment.yaml | kubectl apply -f -

