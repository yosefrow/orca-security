#!/bin/bash -eu

VERSION=$1

repo=orca-app:${VERSION}

docker build -t $repo .
