#!/bin/bash -eu

REGISTRY=515321278346.dkr.ecr.us-east-1.amazonaws.com
REPO=orca-app

function push_local_repo_tag() {
  local repo_tag=$1
  local registry=$2
  local remote_tag=$registry/$repo_tag

  docker tag $repo_tag $remote_tag
  docker push $remote_tag
}


function main() {
  local registry=$REGISTRY
  local repo=$REPO

  local version=$1

  docker tag $repo:$version $repo:latest

  push_local_repo_tag $repo:$version $registry
  push_local_repo_tag $repo:latest $registry 
}

main $@
