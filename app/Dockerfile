FROM python:3.8-slim 

# Based on https://sourcery.ai/blog/python-docker/
# Attempted multi-stage build, but put on hold because run script uses pipenv

# Setup env
ENV LANG C.UTF-8
ENV LC_ALL C.UTF-8
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONFAULTHANDLER 1

# Install pipenv and compilation dependencies
RUN pip install pipenv
RUN apt-get update && apt-get install -y --no-install-recommends gcc 

# Install missing dependencies
RUN apt-get update && apt-get install -y --no-install-recommends python3-psycopg2
RUN apt-get update && apt-get install -y --no-install-recommends python3-flask

# Install utilities # Optionally Remove in production release to optimize
RUN apt-get update && apt-get install -y --no-install-recommends procps net-tools nmap telnet curl

# Create and switch to a new user
RUN useradd --create-home appuser
WORKDIR /home/appuser

# Install application into container
COPY ./src .
RUN PIPENV_VENV_IN_PROJECT=1 pipenv install --deploy
ENV PATH="/.venv/bin:$PATH"

# Install missing dependencies
RUN pipenv install psycopg2-binary 
RUN pipenv install flask

# Don't use root user, for security reasons
#USER appuser

ENV DATABASE_URL ""

# Flask Vars (Not currently working)
#ENV FLASK_RUN_PORT=5000
#ENV FLASK_RUN_HOST=0.0.0.0

# Run the application
CMD ["bash", "./run_app.sh"]
