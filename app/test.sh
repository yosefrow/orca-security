#!/bin/bash -eu

function main() {
  docker-compose down
  docker-compose up -d --build
 
  local time=0
  local max_time=60
  local status=1
  while [ $status != 0 ]; do
    return_code=$(curl -s localhost:5000/_healthz | grep -q '"status": "HEALTHY"'; echo $?)
    status=$return_code
    echo "waiting... ${time}s"
    sleep 5

    time=$((time + 5))
    if [[ $time -ge $max_time ]]; then
      echo Timeout of ${max_time}s exceeded - Test Failed!
      exit 1
    fi
  done

  docker-compose exec orca-app curl localhost:5000/_healthz 
  echo -n "\nTest Completed Successfully"
}

main $@
