#!/bin/bash -eu

function main() {
  source .env
  repo_tag=$BUILD_VERSION
  ./test.sh
  ./scripts/ecr/docker-login.sh
  ./scripts/push.sh $repo_tag
  
  ./scripts/deploy-kubernetes.sh
}

main $@
