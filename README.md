# Orca Security | Scalable logging - exercise

### Description

You are provided with a python service that logs unique access to the service. Your task is to
create a deployment code for this service so it will run on the AWS account provided to you.
The service is designed to be horizontally scalable and is expected to run multiple copies.

### Details
You will receive the code of the service to deploy. The service uses PostgreSQL as a datastore
and expects the URL and the credentials to be passed in the DATABASE_URL environment
variable. The service itself has 2 endpoints (in app.py), one for the service itself and the other
one for the healthcheck.

### We will evaluate the exercise based on:
- Deployment strategy and tools
- Maintainability and durability of the service
- Security
- Packaging of the code

### The expected deliverables are:
- A working environment on the AWS account
- A deployment package (with the deployment script, tools used, etc.)
- A short written summary of the strategy used


## Solution

Kubernetes and docker were chosen to deploy the application due to its ability to scale via HPA and cluster autoscaler

Terraform was selected to deploy K8s and the DB due to its flexiblity and completedness

Dev secrets are to be stored in the .env file for use by docker-compose
`cp .env.example .env` to get started

Prod secrets are to be stored in the .env.prod file (contains DB credentials for templating and prod version)

```
BUILD_VERSION=v0.7
DATABASE_URL=postgresql+psycopg2://user:passed@pg-host:5432/db
```

Deployment secrets are to be configured locally and are ignored by git. Ideally they should be stored in a secrets manager like Vault or CI/CD secrets feature.

The endpoint is currently insecure (http) but in production should use https to avoid packet sniffing of sensitive data.

## Setup

### EKS 

(infra/terraform/eks)

- terraform apply in terraform/eks module
- ./configure-kubectl.sh to get kubectl working

### RDS 

(infra/terraform/rds)

- terraform apply in terraform/rds module
- ./get-connection-url.sh to get connection url
- add output of connection url to .env.prod file 

e.g. DATABASE_URL=postgresql://user/pass@endpoint/dbname

### Docker

./ecr/docker-login.sh to login with aws credentials and output ECR endpoint

add ECR endpoint to kubernetes deployment


### AWS Console (Manual)

The following actions were manually performed, but should be automated as part of infrastructure as code

1. Add VPC Peering for the 2 VPCs (RDS, and EKS)
2. Approve Peering connection for 2 VPCs db-eks-vpc-peering
2. Add Route For RDS subnets to EKS
 - 10.0.0.0/16 -> pcx-0dbee81a983716d66 (db-eks-vpc-peering)
3. Add Route for EKS subnets to RDS
 - 10.99.0.0/18 -> pcx-0dbee81a983716d66 (db-eks-vpc-peering)

Ideally, all infrastructure would be deployed in modules and either a single VPC with multiple subntes, or a dynamic peering connection would be used

### source code changes

app.run in src/app.py was changed to  app.run(host='0.0.0.0'), to allow flask to work work with remote clients

Attempted to use env vars to change environment but did not affect the application. Could have implemented this myself in python, but didn't want to change the source too much as using original source seems to be a requirement.

```
# Flask Vars (Not currently working)
#ENV FLASK_RUN_PORT=5000
#ENV FLASK_RUN_HOST=0.0.0.0
```

### Kubernetes

kubernetes/files are deployed as part of the deploy.sh script

## Components

### Kubernetes

deployed by infra/terraform/eks module

### Database

deployed by infra/terraform/rds module

### ECR Repo

Initialized by app/scripts/ecr

### source

artifact published to ECR using app/src and app/Dockerfile

### Build & Run

- ./test.sh to build and test image locally
- ./deploy.sh to deploy to kubernetes

### kubernetes

- app/kubernetes/files folder : basic templates
- app/helm : work in progress helm chart
